from sixads.api.viewsets import StatisticsViewSet
from rest_framework import routers

router = routers.DefaultRouter()

router.register('statistics',StatisticsViewSet)


