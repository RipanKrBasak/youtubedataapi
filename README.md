# README #
 ### Django Version###
 version: Django 2.0

 ### Python Version ##

 version: >3.5

 ### Django Dependency ###
    pip install django
    pip install djangorestframework
    pip install pygments

 ### App dependency ##
 INSTALLED_APPS = [
    ......
    'rest_framework',
    'django_filters',
    'sixads.apps.SixadsConfig',
]
#### Template ##
 'DIRS': [
            'sixads/templates'
        ],
#### REST framework filter global ###
REST_FRAMEWORK ={
    'DEFAULT_FILTER_BACKENDS':('django_filters.rest_framework.DjangoFilterBackend',)
}

### Url configuration ###


urlpatterns = [
    path('admin/', admin.site.urls),
    path('search/', include('sixads.urls')),
    path('api/', include(router.urls))
]



