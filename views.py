from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from .form import SearchForm

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
from sixads.models import Channel, Statistics

# scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]
api_key = "Api-Key"

api_service_name = "youtube"
api_version = "v3"
youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey=api_key)



def search_tags(request):
    if request.method == "GET":
        form = SearchForm(request.GET)
        if form.is_valid():
            tags = form.cleaned_data["name"]
            channel_id = form.cleaned_data["channel_id"]

            channel_videos_by_tags = get_videos_with_tags(tags=tags,
                                                          channel_id=channel_id)

            # channel_videos_by_tags_stats = get_videos_with_tags_stats(tags="fullname: rob dodson", channel_id="UC_x5XG1OV2P6uZZ5FSM9Ttw")
            video_ids = list(map(lambda x: x['id']['videoId'], channel_videos_by_tags))
            # Return stats with video_ids
            items = get_video_stats(video_ids)

            # items = [
            #     {
            #         "kind": "youtube#video",
            #         "etag": "\"nxOHAKTVB7baOKsQgTtJIyGxcs8/10HDXbNmGv0Q-IKAYIGhTUegwC8\"",
            #         "id": "75EuHl6CSTo",
            #         "statistics": {
            #             "viewCount": "23873",
            #             "likeCount": "157",
            #             "dislikeCount": "3",
            #             "favoriteCount": "0",
            #             "commentCount": "7"
            #         }
            #     },
            #     {
            #         "kind": "youtube#video",
            #         "etag": "\"nxOHAKTVB7baOKsQgTtJIyGxcs8/Yncl7Dg1_SQ5KM1A5gIipcpin1E\"",
            #         "id": "serM0RiWBTk",
            #         "statistics": {
            #             "viewCount": "1114",
            #             "likeCount": "25",
            #             "dislikeCount": "3",
            #             "favoriteCount": "0",
            #             "commentCount": "1"
            #         }
            #     },
            #
            #     {
            #         "kind": "youtube#video",
            #         "etag": "\"nxOHAKTVB7baOKsQgTtJIyGxcs8/VjiGhNeuVLcoFbnUgxJaOi4OTyc\"",
            #         "id": "wf9hZcqQI7A",
            #         "statistics": {
            #             "viewCount": "58997",
            #             "likeCount": "351",
            #             "dislikeCount": "20",
            #             "favoriteCount": "0",
            #             "commentCount": "42"
            #         }
            #     }
            # ]  # Save the data to the table

            if items:

                for item in items:
                    view_count = int(item['statistics']['viewCount'])
                    like_count = int(item['statistics']['likeCount'])
                    dislike_count = int(item['statistics']['dislikeCount'])

                    # print(int(view_count), "***", int(like_count), "****", int(dislike_count), "**", tags)
                    channel_id = Channel(channel=channel_id)
                    channel_id.save()
                    channel_id = Channel.objects.get(pk=1)
                    channel_id.statistics_set.create(video_id=item['id'], view_count=view_count, like_count=like_count,
                                                     dislike_count=dislike_count,
                                                     tags=tags)

                    # return render(request, 'detail.html', {
                    #     'items': items,
                    # })
                return render(request, 'result.html')
        form = SearchForm()

        return render(request, 'search_tags.html', {"form": form})

    else:
        form = SearchForm()

        return render(request, 'search_tags.html', {"form": form})  # ###############


def get_videos_with_tags(tags, channel_id):
    videos = []
    next_page_token = None
    while (1):
        results = youtube.search().list(
            part="snippet",
            channelId=channel_id,
            q=tags,
            maxResults=2,
            type='video',
            pageToken=next_page_token
        ).execute()
        videos += results['items']
        next_page_token = results.get('nextPageToken')
        if next_page_token is None:
            break

    return videos


def get_video_stats(video_ids):
    stats = []
    for i in range(0, len(video_ids), 50):
        res = youtube.videos().list(
            part="statistics",
            id=','.join(video_ids[i:i + 50]),
            fields="items"
        ).execute()

        stats += res['items']
    return stats
